#!/bin/bash
echo "----------------------";
echo Enter file name or file path
read -r file
#word=$(grep "[A-Za-z]+" $file | wc -l)
word=$(grep -Eo '[A-Za-z]+' "$file" | wc -l)
docBlock=$(grep -Eo '(?s)(\/\*(?:(?!\*\/).)+\*\/)\s' "$file" | wc -l)
line=$(grep -Eo '.+' "$file" | wc -l)
#function
int=$(grep -Eo '[0-9]+' "$file" | wc -l)
float=$(grep -Eo '[0-9.\]+' "$file" | wc -l)
intDataType=$(grep -Eo '\$[a-z]+([A-Z][a-z0-9]+)*[ ]*=[ ]*[0-9]+[ ]*;' "$file" | wc -l)
floatDataType=$(grep -Eo'\$[a-z]+([A-Z][a-z0-9]+)*[ ]*=[ ]*[0-9]+\.[0-9]+' "$file" | wc -l)
#@[a-z]+([A-Z][a-z0-9]+)*[ ]int
#diff
#comm
echo Number of words in "$file" is "$word"
echo Number of docBlock in "$file" is "$docBlock"
echo Number of lines in "$file" is "$line"
echo Number of integer in "$file" is "$int"
echo NUmber of float in "$file" is "$float"
echo Number of int data type in "$file" is "$intDataType"
echo Number of float data type in "$file" is "$floatDataType"
