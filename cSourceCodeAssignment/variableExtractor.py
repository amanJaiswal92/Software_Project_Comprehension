import glob
import argparse
import re
import os

reg = "\\"+"b((?:struct\s*.+|define\s*|auto\s*|const\s*|unsigned\s*|signed\s*|register\s*|volatile\s*|static\s*|void\s*|short\s*|long\s*|char\s*|int\s*|float\s*|double\s*|_Bool\s*|complex\s*|bool\s*)+)(?:\s+\*?\*?\s*)([a-zA-Z_][a-zA-Z0-9_]*)\s*(([,][a-zA-Z_][a-zA-Z0-9_]*)+|[\[,;=)])"


def readData(filename):
    fp = open(filename,"r")
    data = fp.read()
    fp.close()
    return data

def writeVariables(filename,data,count):
    fp = open('variables.txt','a')
    fp.write(str(count)+"="*20 + " "*4 + filename + " "*4 + "="*20 + "\n")
    for i in data:
        fp.write(i+"\n")
    fp.close()

def regexMatch(filedata,pattern):
    var = []
    #filedata = ' '.join(filedata.split('\n'))
    filedata = filedata.split('\n')
    for line in filedata:
        temp = re.findall(reg,line) 
        if temp != []:
            temp = " ".join(temp[0]).replace(')','').replace('=','').replace('[','').replace(';','')
            var.append(temp)
    return var

def variables(src):
    src += "/**/*.c"
    pattern = re.compile(reg)
    count =1 
    for filename in glob.glob(src,recursive=True):
        filedata = readData(filename)
        temp = regexMatch(filedata,pattern)
        writeVariables(filename,temp,count) 
        count +=1

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s',dest='src',help="Source file required",required=True)
    args = parser.parse_args() 
    if os.path.exists('variables.txt'):
            os.remove('variables.txt')
    variables(args.src) 

if __name__=="__main__":
    main()
