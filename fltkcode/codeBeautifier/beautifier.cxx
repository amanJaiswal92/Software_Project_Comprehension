
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Text_Editor.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_File_Browser.H>

void copyToOutput(Fl_Widget* obj , void* buffer){
    Fl_Text_Editor *inputDisp = (Fl_Text_Editor*)obj;
    //const char* temp;
    //temp = ((Fl_Text_Editor*)(button->parent()->child(1)))->value();
    buffer = "MY NAME IS KHAN";

    //printf(">>>>>>\n%s\n");
}

void quitProgram(Fl_Widget *o,void*){
    exit(0);
}

void make_window(){
    Fl_Window *win = new Fl_Window(2000,1000,"CODE BEAUTIFIER");
    win->begin();

    //For input display
    Fl_Text_Buffer *inputBuffer = new Fl_Text_Buffer();
    Fl_Text_Editor *inputDisp = new Fl_Text_Editor(50,50,900,800,"INPUT");

    //For output display
    Fl_Text_Buffer *outputBuffer = new Fl_Text_Buffer();
    Fl_Text_Display *outputDisp = new Fl_Text_Display(1000, 50, 950, 800, "OUTPUT");

    // For Buttons
    Fl_Button * submit = new Fl_Button( 900, 900, 70, 30, "Submit" );
    Fl_Button* close = new Fl_Button(990, 900, 70, 30, "Quit");
     
    win->end();
   
    inputDisp ->buffer(inputBuffer);
    win->resizable(*inputDisp);

    outputDisp->buffer(outputBuffer);
    win->resizable(*outputDisp);

    submit -> callback(copyToOutput,outputBuffer);
 
   //outputDisp->buffer(outputBuffer);
   // win->resizable(*outputDisp);


    close -> callback(quitProgram);
    win->show();
    
}

int main() {
    make_window();
    return Fl::run();
}

